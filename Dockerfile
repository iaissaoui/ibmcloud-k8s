## 1. Universal Base Image (UBI).
FROM registry.access.redhat.com/ubi8

## 2. Non-root, arbitrary user IDs
#USER 1001
# Or USER default; or nothing, the UBI already set the user

## 6. Image identification
LABEL name="ia/ibmcloud-ks" \
      vendor="me" \
      version="latest" \
      release="latest" \
      summary="Simple wrapper adding ibmcloud and kubectl to ubi8 image" \
      description="Simple wrapper adding ibmcloud and kubectl to ubi8 image"

USER root

## 7. Image license
#COPY ./licenses /licenses

## 5. Latest security updates
RUN dnf -y update-minimal --security --sec-severity=Important --sec-severity=Critical && \
    dnf clean all
## kubectl
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/local/bin/kubectl
## ibmcloud
RUN curl -fsSL https://clis.cloud.ibm.com/install/linux | sh
RUN ibmcloud plugin install container-service
RUN ibmcloud plugin install container-registry
RUN ibmcloud plugin install observe-service
#USER 1001
COPY entrypoint.sh entrypoint.sh
RUN cat entrypoint.sh
#USER root
RUN chmod +x entrypoint.sh
#USER 1001

ENTRYPOINT ["./entrypoint.sh"]