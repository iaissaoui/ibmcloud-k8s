# ibmcloud-ks

Simple image to connect to your ibmcloud k8s cluster

## Getting started

## Pull image
docker pull registry.gitlab.com/iaissaoui/ibmcloud-k8s

## Run image
docker run -it --env IBMCLOUD_APIKEY=<YOUR_IBMCLOUD_APIKEY> --env IBMCLOUD_CLUSTER=<YOUR_IBMCLOUD_CLUSTER>  regi
stry.gitlab.com/iaissaoui/ibmcloud-k8s
